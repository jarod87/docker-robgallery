FROM node:6.5.0

MAINTAINER Daniel Toth <firius87@gmail.com>

RUN mkdir /src

RUN npm install -g bower grunt-cli

WORKDIR /src
ADD ./src/ /src/

ENV GIT_DIR=.git/modules/src

RUN npm install
RUN bower install --allow-root
RUN grunt

ENV NODE_ENV=production

RUN npm prune --production

EXPOSE 3000

CMD npm start
